from Tkinter import *
import subprocess

def refresh():
    T.delete(0.0, END)
    result = subprocess.Popen(['sensors'],  stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    out, err = result.communicate()
    T.insert(END, out)
    root.after(5000, refresh)

root = Tk()
T = Text(root, height=15, width=60)
T.pack()
refresh()
mainloop()
