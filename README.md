# sensors
Displayed temperature data from the sensors application.

1. Install tkinter
  ```sudo apt-get install python-tk```

2. Install lm-sensors
  ```sudo apt-get install lm-sensors```

3. After installation type the following in terminal
  ```sudo sensors-detect```

4. You may also need to run
  ```sudo service kmod start```

It will ask you few questions. Answer Yes for all of them. Finally run ```python sensors.py```